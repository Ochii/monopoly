<?php

/**
 * Classe Card
 * 
 * @category Card
 * @package  Entity
 * @author   Mathieu MESNIL <mathieu.mesnil@campus.academy>
 * @author   Guillaume Duarte <guillaume.duarte@campus.academy>
 * @license  https://M&G.campus.academy.fr M&G
 * @link     https://M&G.campus.academy.fr
 */
class Card
{
    /**
     * Descriptif de la carte
     *
     * @var string
     */
    public $description;
        
    /**
     * Valeur de la carte
     *
     * @var int
     */
    public $value;
    
    /**
     * __construct
     *
     * @param array $data   
     */
    public function __construct(array $data)
    {
        $this->description = $data['description'];
        $this->value = $data['value'];
    }
}
