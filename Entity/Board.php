<?php

require_once "../Entity/Board.php";
require_once "../Entity/Card.php";
require_once "../Entity/Box.php";
require_once "../Data/BoxData.php";
require_once "../Data/CardData.php";

/**
 * Class Board
 * 
 * @category Board
 * @package  Entity
 * @author   Mathieu MESNIL <mathieu.mesnil@campus.academy>
 * @author   Guillaume Duarte <guillaume.duarte@campus.academy>
 * @license  https://M&G.campus.academy.fr M&G
 * @link     https://M&G.campus.academy.fr
 */
class Board
{
    /**
     * Constante du nombre de case du plateau
     */
    public const NUMBER_OF_BOX = 41;
    /**
     * Box
     *
     * @var array
     */
    public $box;
    /**
     * Cards
     *
     * @var array
     */
    public $cards;

    /**
     * Players
     *
     * @var array
     */
    public $players;

    /**
     * NbHouseFree
     *
     * @var int
     */
    public $nbHouseFree = 32;

    /**
     * NbHotelFree
     *
     * @var int
     */
    public $nbHotelFree = 16;


    /**
     * Premier dé
     *
     * @var null
     */
    public $dice1 = null;

    /**
     * Deuxième dé
     *
     * @var null
     */
    public $dice2 = null;

    /**
     * __construct
     *
     * @param array $players tableau avec les joueurs
     */
    public function __construct(Array $players)
    {
        $boxData = new BoxData;
        foreach ($boxData->data as $box) {
            $this->box[] = new Box($box);
        }
        $cardData = new CardData;
        foreach ($cardData->data as $card) {
            $this->cards[] = new Card($card);
        }
        $this->players = $players;
        //Mélange des cartes
        // $this->cards->shuffle();
    }

    /**
     * SetNbHouseFree : Auto incrément ou décrément
     *
     * @param int $sellOrBuy 
     * 
     * @return void
     */
    public function setNbHouseFree(int $sellOrBuy): void
    {
        $this->nbHouseFree += $sellOrBuy;
    }

    /**
     * SetNbHotelFree : Auto incrément ou décrément
     *
     * @param int $sellOrBuy 
     * 
     * @return void
     */
    public function setNbHotelFree(int $sellOrBuy): void
    {
        $this->nbHotelFree += $sellOrBuy;
    }

    /**
     * Lancé de dés
     * 
     * @throws Exception
     * 
     * @return void
     */
    public function rollDice(): void
    {
        $this->dice1 = random_int(1, 6);
        $this->dice2 = random_int(1, 6);
    }

    /**
     *  Déplacement du joueur sur le plateau
     * 
     * @param $player 
     * @param int $movement 
     * 
     * @return bool
     */
    public function movePlayer(Player $player, int $movement)
    {
        if (get_class($player) == "Player") {
            $player->placement = $player->placement + ($movement);
            // dans le cas ou on passe par la case départ
            if ($player->placement > self::NUMBER_OF_BOX) {
                $player->placement = $player->placement - self::NUMBER_OF_BOX;
                return true;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Passage par la case départ
     * 
     * @param $placement 
     * @param $movement 
     * 
     * @return bool
     */
    public function passedByStartBox($placement, $movement)
    {
        return $placement + $movement > self::NUMBER_OF_BOX ? true : false;
    }

    /**
     * Check du status de la box
     * 
     * @param int $placement 
     * 
     * @return mixed
     */
    public function typeOfBox(int $placement)
    {
        return $this->box[$placement]->status;
    }

    /**
     * DrawCard : fonction pour piocher une carte aléatoirement
     *            Ajouter le montant de la carte au joueur
     *            Renvoyer le descriptif de la carte pour affichage
     * 
     * @param Player $player 
     * 
     * @return string
     */
    public function drawCard(&$player)
    {
        // Si le tableau de carte est vide on le rempli
        if (!$this->cards) {
            $cardData = new CardData;
            foreach ($cardData->data as $card) {
                $this->cards[] = new Card($card);
            }
        }
        // Piocher aléatoirement
        $index = rand(0, 12);
        $card = $this->cards[$index];
        // Suppression de la carte piochée
        unset($this->cards[$index]);
        // Ajout du montant de la carte au joueur
        $player->money += $card['value'];

        return $card->description;
    }

}
?>

