<?php

/**
 * Classe Box
 * 
 * @category Box
 * @package  Entity
 * @author   Mathieu MESNIL <mathieu.mesnil@campus.academy>
 * @author   Guillaume Duarte <guillaume.duarte@campus.academy>
 * @license  https://M&G.campus.academy.fr M&G
 * @link     https://M&G.campus.academy.fr
 */
class Box
{
    /**
     * Placement emplacement de la case sur le plateau
     *
     * @var int
     */
    public $placement;

    /**
     * Name : nom de la case
     *
     * @var string
     */
    public $name;

    /**
     * Status :
     *          0 = case parc gratuit
     *          1 = case chance / caisse communauté
     *          2 = case rue
     *          3 = case compagnie
     *          4 = case gare
     *          5 = case prison
     *          6 = case impot sur le revenu
     *          7 = case en hypothèque
     *
     * @var int
     */
    public $status;

    /**
     * NumberOfBuildingSet : nombre de bâtiment construit sur la case.
     *                      (valeur par défaut 0)
     *
     * @var int
     */
    public $numberOfBuildingSet = 0;

    /**
     * BuildingPrice : Prix pour achat d'un bâtiment
     *                 (valeur par défaut 0 pour les cases ne pouvant en avoir)
     *
     * @var int
     */
    public $buildingPrice = 0;

    /**
     * BoxPrice : prix d'achat de la case si achetable
     *          (valeur par défaut 0 pour les cases ne pouvant être achetée)
     *
     * @var int
     */
    public $boxPrice = 0;

    /**
     * MortGage : montant d'hypothèque
     *          (valeur par défaut 0 pour les cases ne pouvant être achetée)
     *
     * @var int
     */
    public $mortGage = 0;

    /**
     * BoxRent : montant du loyer de base de la case
     *          (valeur par défaut 0 pour les cases sans loyer)
     *
     * @var int
     */
    public $boxRent = 0;

    /**
     * Group : identifiant du groupe auquel appartient la case
     *
     * @var int
     */
    public $group = null;

    /**
     * Owner : propriétaire de la case
     *          (valeur par défaut null pour les cases ne pouvant être achetée)
     *
     * @var \Player
     */
    public $owner = null;

    /**
     * __construct
     *
     * @param array $data tableau de données
     */
    public function __construct(array $data)
    {
        $this->placement = $data["placement"];
        $this->name = $data["name"];
        $this->status = $data["status"];
        // Valable uniquement pour les cases constructible
        if (isset($data["buildingPrice"])) {
            $this->buildingPrice = $data["buildingPrice"];
        }
        // Valable uniquement pour les cases achetable
        if (isset($data["boxPrice"])) {
            $this->boxPrice = $data["boxPrice"];
            $this->mortGage = $data["mortGage"];
            $this->boxRent = $data["boxRent"];
            $this->group = $data["group"];
        }
    }

    /**
     * Calcul du loyer des cartes gares
     *
     * @param int $multiplier 
     *
     * @return int
     */
    public function rentForStation(int $multiplier)
    {
        return $this->boxRent * $multiplier;
    }

    /**
     * Calcul du loyer des cartes compagnie eau/elec.
     *
     * @param int $dice 
     * @param int $multiplier 
     *
     * @return int
     */
    public function rentForCompany(int $dice, int $multiplier = 1)
    {
        if ($multiplier == 1) {
            return $dice * 4;
        } else {
            return $dice * 10;
        }
    }

    /**
     * Calcul du loyer des cartes street
     *
     * @param int $multiplier 0 si le propriétaire n'a pas toutes les cases ou 2
     *
     * @return int
     */
    public function rentForStreet(int $multiplier): int
    {
        // Si le propriétaire possède toutes les cartes de la même couleur
        // mais pas de bâtiment sur la case déclenchée alors loyer de base x2
        if ($multiplier) {
            return $this->boxRent * 2;
        }
        // Si il y a un ou plusieurs bâtiment sur la case
        if ($this->numberOfBuildingSet) {
            $exp = 2 - ($this->boxRent / 100);
            return round($this->boxRent*(pow($this->numberOfBuildingSet, $exp)*5));
        }
        return $this->boxRent;
    }


    /**
     * Permit to check how many boxes the player in the group
     *
     * @param array $boxList 
     *
     * @return int
     */
    public function checkOwnerForCompanyOrStation(array $boxList): int
    {
        $return = 0;
        $boxStatus = $this->status;
        foreach ($boxList as $i => $item) {
            if (get_class($item) == "Box") {
                // on verifie que les Boxes sont du même groupe 
                // et que le status correspond bien a une compagnie ou gare
                if ($this->group === $item->group 
                    && ($boxStatus === 3 || $boxStatus === 4)
                ) {
                    if ($this->owner != $item->owner) {
                        $return++;
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Permit to check if the player got all street Boxes in the group
     *
     * @param array $boxList 
     * 
     * @return bool
     */
    public function checkOwnerGotAllStreetBoxes(array $boxList): bool
    {
        $return = false;
        foreach ($boxList as $i => $item) {
            if (get_class($item) == "Box") {
                if ($this->group === $item->group) {
                    // on verifie que les Boxes sont du même groupe 
                    // et que le status correspond bien a une rue
                    if ($this->owner != $item->owner && $item->status == 2) {
                        $return = true;
                    }
                }
            }
        }
        return $return;
    }

    /**
     * SellBuilding : fonction de vente des bâtiments présents sur une case
     *
     * @param int $numberOfBuildingToSell 
     *
     * @return void
     */
    public function sellBuilding(int $numberOfBuildingToSell): void
    {
        if (($this->numberOfBuildingSet - $numberOfBuildingToSell) >= 0) {
            $this->numberOfBuildingSet -= $numberOfBuildingToSell;
        }
    }
}
