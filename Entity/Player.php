<?php

/**
 * Player
 * 
 * @category Player
 * @package  Entity
 * @author   Mathieu MESNIL <mathieu.mesnil@campus.academy>
 * @author   Guillaume Duarte <guillaume.duarte@campus.academy>
 * @license  https://M&G.campus.academy.fr M&G
 * @link     https://M&G.campus.academy.fr
 */
class Player
{
    /**
     * Id : identifiant du joueur utilisé pour les groupements de box
     *
     * @var int
     */
    public $id;

    /**
     * Name : nom du joueur
     *
     * @var string
     */
    public $name;

    /**
     * Money : initialisé à 1500€ pour le démarrage
     *
     * @var int
     */
    public $money = 1500;
 
    /**
     * Placement : emplacement du joueur sur le plateau
     *
     * @var int
     */
    public $placement = 0;
    
    /**
     * InPrison : Status à 0 quand tout va bien. Set à 3 si envoyé en prison
     *           Sera décrémenté à chaque tour si != 0
     *
     * @var int
     */
    public $inPrison = 0;
    
    /**
     * __construct
     *
     * @param int    $id   identifiant
     * @param string $name nom du joueur
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}
