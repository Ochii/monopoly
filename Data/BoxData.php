<?php

/**
 * Stockage des données des cases
 * 
 * @category BoxData
 * @package  Data
 * @author   Mathieu MESNIL <mathieu.mesnil@campus.academy>
 * @author   Guillaume Duarte <guillaume.duarte@campus.academy>
 * @license  https://M&G.campus.academy.fr M&G
 * @link     https://M&G.campus.academy.fr
 */
class BoxData
{
    
    /**
     * Data : données souche
     *
     * @var array
     */
    public $data = [
        0 => [
           "placement" => 0,
           "name" => "Case départ",
           "status" => 0,
        ],
        1 => [
            "placement" => 1,
            "name" => "Belleville",
            "status" => 1,
            "buildingPrice" => 50,
            "boxPrice" => 60,
            "mortGage" => 30,
            "boxRent" => 2,
            "group" => 1
        ],
        2 => [
            "placement" => 2,
            "name" => "Caisse communauté",
            "status" => 0,
        ],
        3 => [
            "placement" => 3,
            "name" => "Lecourbe",
            "status" => 1,
            "buildingPrice" => 50,
            "boxPrice" => 60,
            "mortGage" => 30,
            "boxRent" => 4,
            "group" => 1
        ],
        4 => [
            "placement" => 4,
            "name" => "Impôt",
            "status" => 0,
        ],
        5 => [
            "placement" => 5,
            "name" => "Gare Montparnasse",
            "status" => 1,
            "boxPrice" => 200,
            "mortGage" => 100,
            "boxRent" => 25,
            "group" => 2
        ],
        6 => [
            "placement" => 6,
            "name" => "Vaugirard",
            "status" => 1,
            "buildingPrice" => 50,
            "boxPrice" => 100,
            "mortGage" => 50,
            "boxRent" => 6,
            "group" => 3
        ],
        7 => [
            "placement" => 7,
            "name" => "Carte Chance",
            "status" => 0,
        ],
        8 => [
            "placement" => 8,
            "name" => "Courcelles",
            "status" => 1,
            "buildingPrice" => 50,
            "boxPrice" => 100,
            "mortGage" => 50,
            "boxRent" => 6,
            "group" => 2
        ],
        9 => [
            "placement" => 9,
            "name" => "Républiques",
            "status" => 1,
            "buildingPrice" => 50,
            "boxPrice" => 120,
            "mortGage" => 60,
            "boxRent" => 8,
            "group" => 2
        ],
        10 => [
            "placement" => 10,
            "name" => "Prison",
            "status" => 0,
        ],
        11 => [
            "placement" => 11,
            "name" => "La Vilette",
            "status" => 1,
            "buildingPrice" => 200,
            "boxPrice" => 300,
            "mortGage" => 150,
            "boxRent" => 26,
            "group" => 3
        ],
        12 => [
            "placement" => 12,
            "name" => "Compagnie électricité",
            "status" => 1,
            "boxPrice" => 150,
            "mortGage" => 75,
            "boxRent" => 0,
            "group" => 4
        ],
        13 => [
            "placement" => 13,
            "name" => "Neuilly",
            "status" => 1,
            "buildingPrice" => 200,
            "boxPrice" => 300,
            "mortGage" => 150,
            "boxRent" => 26,
            "group" => 3
        ],
        14 => [
            "placement" => 14,
            "name" => "Paradis",
            "status" => 1,
            "buildingPrice" => 200,
            "boxPrice" => 320,
            "mortGage" => 160,
            "boxRent" => 28,
            "group" => 3
        ],
        15 => [
            "placement" => 15,
            "name" => "Gare de Lyon",
            "status" => 1,
            "boxPrice" => 200,
            "mortGage" => 100,
            "boxRent" => 25,
            "group" => 2
        ],
        16 => [
            "placement" => 16,
            "name" => "Mozart",
            "status" => 1,
            "buildingPrice" => 100,
            "boxPrice" => 140,
            "mortGage" => 70,
            "boxRent" => 10,
            "group" => 5
        ],
        17 => [
            "placement" => 17,
            "name" => "Caisse communauté",
            "status" => 0,
        ],
        18 => [
            "placement" => 18,
            "name" => "Saint Michel",
            "status" => 1,
            "buildingPrice" => 100,
            "boxPrice" => 140,
            "mortGage" => 70,
            "boxRent" => 10,
            "group" => 5
        ],
        19 => [
            "placement" => 19,
            "name" => "Pigalle",
            "status" => 1,
            "buildingPrice" => 100,
            "boxPrice" => 160,
            "mortGage" => 80,
            "boxRent" => 12,
            "group" => 5
        ],
        20 => [
            "placement" => 20,
            "name" => "Parc Gratuit",
            "status" => 0,
        ],
        21 => [
            "placement" => 21,
            "name" => "Matignon",
            "status" => 1,
            "buildingPrice" => 150,
            "boxPrice" => 220,
            "mortGage" => 110,
            "boxRent" => 18,
            "group" => 6
        ],
        22 => [
            "placement" => 22,
            "name" => "Carte chance",
            "status" => 0,
        ],
        23 => [
            "placement" => 23,
            "name" => "Malesherbes",
            "status" => 1,
            "buildingPrice" => 150,
            "boxPrice" => 220,
            "mortGage" => 110,
            "boxRent" => 18,
            "group" => 6
        ],
        24 => [
            "placement" => 24,
            "name" => "Henri-martin",
            "status" => 1,
            "buildingPrice" => 150,
            "boxPrice" => 240,
            "mortGage" => 120,
            "boxRent" => 20,
            "group" => 6
        ],
        25 => [
            "placement" => 25,
            "name" => "Gare du Nord",
            "status" => 1,
            "boxPrice" => 200,
            "mortGage" => 100,
            "boxRent" => 25,
            "group" => 2
        ],
        26 => [
            "placement" => 26,
            "name" => "Saint Honoré",
            "status" => 1,
            "buildingPrice" => 150,
            "boxPrice" => 260,
            "mortGage" => 130,
            "boxRent" => 22,
            "group" => 7
        ],
        27 => [
            "placement" => 27,
            "name" => "La Bourse",
            "status" => 1,
            "buildingPrice" => 150,
            "boxPrice" => 260,
            "mortGage" => 130,
            "boxRent" => 22,
            "group" => 7
        ],
        28 => [
            "placement" => 28,
            "name" => "Compagnie des eaux",
            "status" => 1,
            "boxPrice" => 150,
            "mortGage" => 75,
            "boxRent" => 0,
            "group" => 4
        ],
        29 => [
            "placement" => 29,
            "name" => "La Fayette",
            "status" => 1,
            "buildingPrice" => 150,
            "boxPrice" => 280,
            "mortGage" => 140,
            "boxRent" => 24,
            "group" => 7
        ],
        30 => [
            "placement" => 30,
            "name" => "Allez en prison",
            "status" => 0,
        ],
        31 => [
            "placement" => 31,
            "name" => "Breteuil",
            "status" => 1,
            "buildingPrice" => 100,
            "boxPrice" => 180,
            "mortGage" => 90,
            "boxRent" => 14,
            "group" => 8
        ],
        32 => [
            "placement" => 32,
            "name" => "Foch",
            "status" => 1,
            "buildingPrice" => 100,
            "boxPrice" => 180,
            "mortGage" => 90,
            "boxRent" => 14,
            "group" => 8
        ],
        33 => [
            "placement" => 33,
            "name" => "Caisse communauté",
            "status" => 0,
        ],
        34 => [
            "placement" => 34,
            "name" => "Capucines",
            "status" => 1,
            "buildingPrice" => 100,
            "boxPrice" => 200,
            "mortGage" => 100,
            "boxRent" => 16,
            "group" => 8
        ],
        35 => [
            "placement" => 35,
            "name" => "Gare Saint-Lazare",
            "status" => 1,
            "boxPrice" => 200,
            "mortGage" => 100,
            "boxRent" => 25,
            "group" => 2
        ],
        36 => [
            "placement" => 36,
            "name" => "Carte chance",
            "status" => 0,
        ],
        37 => [
            "placement" => 37,
            "name" => "Champs-Elysés",
            "status" => 1,
            "buildingPrice" => 200,
            "boxPrice" => 350,
            "mortGage" => 175,
            "boxRent" => 35,
            "group" => 9
        ],
        38 => [
            "placement" => 38,
            "name" => "Taxe de Luxe",
            "status" => 0,
        ],
        39 => [
            "placement" => 37,
            "name" => "La Paix",
            "status" => 1,
            "buildingPrice" => 200,
            "boxPrice" => 400,
            "mortGage" => 200,
            "boxRent" => 50,
            "group" => 9
        ],
    ];
};