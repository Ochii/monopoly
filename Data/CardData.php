<?php

/**
 * Stockage des données des cartes chances et communautaire
 * 
 * @category CardData
 * @package  Data
 * @author   Mathieu MESNIL <mathieu.mesnil@campus.academy>
 * @author   Guillaume Duarte <guillaume.duarte@campus.academy>
 * @license  https://M&G.campus.academy.fr M&G
 * @link     https://M&G.campus.academy.fr
 */
class CardData
{
    /**
     * Data : données sources des cartes chances
     *            et caisse communautés
     *
     * @var array
     */
    public $data = [
        0 => [
           "description" => "Parce que le monde est injuste, vous payez 5€",
           "value" => -5,
        ],
        1 => [
            "description" => "Vous devez racheter des clopes, vous payez 24€",
            "value" => -24,
        ],
        2 => [
            "description" => "Il restait de l'argent dans la tirelire de votre enfant
            , vous gagnez 10€",
            "value" => 10,
         ],
         3 => [
            "description" => "Vous héritez de mamie la pince, vous gagnez 25€",
            "value" => 25,
         ],
         4 => [
            "description" => "Vous avez résilié votre abonnement trop tôt,
                             vous payez 15€ de frais de dossier",
            "value" => -15,
         ],
         5 => [
            "description" => "Les impôts vous ont retrouvé ! Vous payez 50€",
            "value" => -50,
         ],
         6 => [
            "description" => "Vous avez joué 30€ de ticket à gratter, vous gagnez 2€ 
                            (on oublie vite l'investissement d'origine)",
            "value" => 2,
         ],
         7 => [
            "description" => "Vous vous êtes fait scanner votre CB dans le métro,
             vous perdez 60€",
            "value" => -60,
         ],
         8 => [
            "description" => "Parce que parfois la roue tourne doit tourner,
                             vous gagnez 80€ (Merci Riberi)",
            "value" => 80,
         ],
         9 => [
            "description" => "Les comptes premium de Brazzer ont été piratés,
             vous perdez 20€",
            "value" => -20,
         ],
         10 => [
            "description" => "Vous êtes confiné depuis 55 jours,
             vous économisez 200€",
            "value" => 200,
         ],
         11 => [
            "description" => "Fin du confinement vous sortez fêter ça,
             vous payez 50€",
            "value" => -50,
         ],
         12 => [
            "description" => "Vous êtes toujours entrain de jouer à ce jeu ???
            Pour la peine on vous donne 1000€",
            "value" => 1000,
         ]
    ];
}